const express = require ("express");
const router = express.Router();
const {getAll} = require('../db/conexion');
const fs = require('fs');
const multer = require('multer');
const upload = multer({ dest: './public/images' })
const fileUpload = upload.single('src');
const sqlite3 = require('sqlite3');
const path = require('path');



router.get("/", async (req, res) => {
    res.render("admin/index", {   
    });
});
router.get("/integrantes/listar", async (req, res) => {
    const integrantes = await getAll("select * from integrantes");
    console.log("INTEGRANTES", integrantes);

    const matricula = req.params.matricula;
    console.log(matricula)
    //if (matriculas.includes(matricula)) {
        const integrantesFilter=await getAll ("select * from integrantes where matricula = ?", [matricula]);
        //const integrantesList=await getAll ("select * from integrantes",);
        const media2Filter=await getAll ("select * from media2 where matricula = ?", [matricula]);
        res.render("admin/integrantes/index", {  
            integrantes:integrantes, 
        });
    
});
router.get('/media/listar',async (req,res) => {
    const media = await getAll("select * from media");
    console.log("media", media);

    const Tmedia = req.params.Tmedia;
    console.log(Tmedia)
    const integrantesFilter=await getAll ("select * from media", [Tmedia]);
    res.render('admin/media/index',{
        media:media,
    });

})
router.get('/media2/listar',async(req,res) => {
    const media2 = await getAll("select * from media2");
    console.log("media", media2);

    const medios = req.params.medios;
    console.log(medios)
    const integrantesFilter=await getAll ("select * from media2 GROUP BY matricula", [medios]);
    res.render('admin/media2/index',{
        media2:media2,
    });
})

router.get("/integrantes/crear",async(req,res) =>{
    res.render('admin/integrantes/crearForm');
})
router.get("/media2/crear",async(req,res) =>{
    res.render('admin/media2/crearForm');
})

router.post('/integrantes/create', async (req, res) => {
    try {
        const { matricula, nombre, apellido } = req.body;
        db.run(
            "INSERT INTO Integrantes (matricula, nombre, apellido) VALUES (?, ?, ?)",
            [matricula, nombre, apellido],
            (err) => {
                if (err) {
                    console.error("Error al insertar en la base de datos:", err);
                    return res.sendStatus(500);
                }
                res.redirect("/admin/Integrante/listar");
            }
        );
    } catch (error) {
        console.error('Error al insertar en la base de datos:', error);
        res.status(500).send('Error al insertar en la base de datos');
    }
});

router.post('/media2/create', fileUpload, async (req, res) => {
    try {
        const { nombre, src, url, matricula,  titulo} = req.body;
        let imagenPath = '';
        let embedUrl = '';

        if (req.file) {
            const nombreArchivo = titulo.replace(/\s+/g, '-').toLowerCase();
            const extension = path.extname(req.file.originalname);
            const nuevoNombreArchivo = `${nombreArchivo}-${Date.now()}${extension}`;
            
            fs.renameSync(req.file.path, path.join(req.file.destination, nuevoNombreArchivo));
            imagenPath = path.join(req.file.destination, nuevoNombreArchivo).replace(/\\/g, '/');
        }

        if (video) {
            // Convert the video URL to embed format
            const videoIdMatch = video.match(/(?:https?:\/\/)?(?:www\.)?(?:youtube\.com\/(?:watch\?v=|embed\/)|youtu\.be\/)([^\s&]+)/);
            if (videoIdMatch) {
                const videoId = videoIdMatch[1];
                embedUrl = `https://www.youtube.com/embed/${videoId}`;
            } else {
                return res.redirect('/admin/media2/crear?mensaje=URL de YouTube no válida');
            }
        }

        if (!nombre || !src || !url || !matricula || !titulo) {
            return res.redirect('/admin/Media/crear?mensaje=Todos los campos son obligatorios');
        }

        

        

        db.run(
            "INSERT INTO media2 (nombre, src, url, matricula, titulo) VALUES (?, ?, ?, ?, ?)",
            [nombre, src, url, matricula, titulo],
            (err) => {
                if (err) {
                    console.error("Error al insertar en la base de datos:", err);
                    return res.sendStatus(500);
                }
                res.redirect("/admin/media2/listar");
            }
        );
    } catch (error) {
        console.error('Error al insertar en la base de datos:', error);
        res.status(500).send('Error al insertar en la base de datos');
    }
});




module.exports = router;