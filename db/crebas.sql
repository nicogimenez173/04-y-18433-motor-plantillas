CREATE TABLE IF NOT EXISTS "integrantes" (
	"matricula" TEXT NOT NULL UNIQUE,
	"nombre" TEXT,
	"apellido" TEXT,
	PRIMARY KEY("matricula")	
);

CREATE TABLE IF NOT EXISTS "media" (
	"id" INTEGER NOT NULL UNIQUE,
	"nombre" TEXT,
	PRIMARY KEY("id")
);

CREATE TABLE IF NOT EXISTS "media2" (
	"id" INTEGER NOT NULL UNIQUE,
	"nombre" TEXT,
	"url" TEXT,
	"titulo" TEXT,
	"matricula" TEXT,
	PRIMARY KEY("id"),
	FOREIGN KEY ("matricula") REFERENCES "integrantes"("matricula")
	ON UPDATE NO ACTION ON DELETE NO ACTION
    FOREIGN KEY ("id") REFERENCES "media"("idmedia")
	ON UPDATE NO ACTION ON DELETE NO ACTION
);
